from fastapi import FastAPI
from resources.amortization_system import router as amortization_system_resource
from resources.retirement_planning import router as retirement_planning_resource

description = """
A API Finance Calculator ajuda você a fazer coisas incríveis. 🚀
"""

app = FastAPI(
    title="Finance Calculator",
    description=description,
    version="0.0.3",
    contact={
        "name": "GITLAB",
        "url": "https://gitlab.com/geanduarteucont/finance-calculator-api/",
        "email": "gean@ucont.com.br",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)


app.include_router(amortization_system_resource)
app.include_router(retirement_planning_resource)
