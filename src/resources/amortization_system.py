from fastapi.routing import APIRouter
from controller import AmortizationSystemController
from contracts import AmortizationSystemContract


router = APIRouter(prefix="/simulate/amortization_system")


@router.post("", status_code=201)
async def create_simulation(contract: AmortizationSystemContract):
    as_controller = AmortizationSystemController()
    amortization_system = await as_controller.simulate_by_contract(contract)
    return amortization_system.dict()
