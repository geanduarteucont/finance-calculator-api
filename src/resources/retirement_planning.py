from fastapi.routing import APIRouter
from controller import RetirementPlanningController
from contracts import RetirementPlanningContract


router = APIRouter(prefix="/simulate/retirement_planning")


@router.post("", status_code=201)
async def create_simulation(contract: RetirementPlanningContract):
    retirement_controller = RetirementPlanningController()
    retirement_planning = await retirement_controller.simulate_by_contract(contract)
    return retirement_planning.dict()
