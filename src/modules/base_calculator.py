class BaseCalculator:
    def get_interest(self, balance: float, rate: float) -> float:
        return balance * rate

    def get_payment(
        self,
        present_value: float,
        number_of_periods: float,
        rate: float,
        series_payments: str = "end",
    ) -> float:

        if series_payments == "end":
            return present_value / (
                (((1 + rate) ** number_of_periods) - 1)
                / (((1 + rate) ** number_of_periods) * rate)
            )

        if series_payments == "begin":
            return (
                self.get_present_value(
                    future_value=present_value,
                    rate=rate,
                    number_of_periods=number_of_periods,
                )
            ) / (((1 - (1 + rate) ** (-number_of_periods)) / rate) * (1 + rate))

    def get_equivalent_rate(
        self, rate: float, given_time: int, requested_time: int
    ) -> float:
        time_ratio = requested_time / given_time
        return ((1 + rate) ** time_ratio) - 1

    def get_present_value(
        self,
        future_value: float = None,
        payment: float = None,
        rate: float = 0,
        number_of_periods: int = 0,
        series_payments: str = "end",
    ):
        if future_value is None and series_payments == "end":
            return payment * (
                (((1 + rate) ** number_of_periods) - 1)
                / (((1 + rate) ** number_of_periods) * rate)
            )

        if payment is None:
            return future_value / ((1 + rate) ** number_of_periods)

    def get_number_of_periods(self):
        ...

    def get_rate(self):
        ...

    def get_future_value(
        self, present_value: float, rate: float, number_of_periods: int
    ) -> float:
        return present_value * (1 + rate) ** number_of_periods
