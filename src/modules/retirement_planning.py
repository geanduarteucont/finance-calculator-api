from modules.base_calculator import BaseCalculator
from contracts import RetirementPlanningContract, RetirementPlanningContractResponse


class RetirementPlanning(BaseCalculator):
    async def get_monthly_deposits(
        self: object, contract: RetirementPlanningContract
    ) -> RetirementPlanningContractResponse:
        number_months_until_retirement = (
            contract.retiring_age - contract.current_age
        ) * 12
        number_months_to_end_of_life = (
            contract.life_expectancy_age - contract.retiring_age
        ) * 12
        expenses_standard_living = contract.expenses_standard_living
        investment_rate = contract.investment_rate
        retirement_rate = contract.retirement_rate

        amount_needed_to_accumulate = self.get_present_value(
            payment=expenses_standard_living,
            rate=retirement_rate,
            number_of_periods=number_months_to_end_of_life,
        )

        monthly_deposit = self.get_payment(
            present_value=amount_needed_to_accumulate,
            number_of_periods=number_months_until_retirement,
            rate=investment_rate,
            series_payments="begin",
        )

        contract_response = RetirementPlanningContractResponse(
            current_age=contract.current_age,
            retiring_age=contract.retiring_age,
            life_expectancy_age=contract.life_expectancy_age,
            expenses_standard_living=contract.expenses_standard_living,
            retirement_rate=contract.retirement_rate,
            investment_rate=contract.investment_rate,
            monthly_deposit=round(monthly_deposit, 2),
        )

        return contract_response
