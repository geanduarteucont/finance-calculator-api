from modules.base_calculator import BaseCalculator
from contracts import (
    AmortizationSystemContract,
    AmortizationSystemContractResponse,
    Row,
)


class AmortizationSystem(BaseCalculator):
    async def get_price_system(
        self: object, contract: AmortizationSystemContract
    ) -> AmortizationSystemContractResponse:
        amortization_system = AmortizationSystemContractResponse(
            system_type=contract.system_type,
            present_value=contract.present_value,
            number_of_periods=contract.number_of_periods,
            rate=contract.rate,
            number_of_grace_periods=contract.number_of_grace_periods,
        )

        if contract.number_of_grace_periods > 0:
            present_value = self.get_future_value(
                present_value=contract.present_value,
                rate=contract.rate,
                number_of_periods=contract.number_of_grace_periods,
            )

            payment = self.get_payment(
                present_value, contract.number_of_periods, contract.rate
            )

        else:

            payment = self.get_payment(
                contract.present_value, contract.number_of_periods, contract.rate
            )
        balance = contract.present_value

        total_periods = contract.number_of_periods + contract.number_of_grace_periods
        for period in range(1, total_periods + 1):
            if period <= contract.number_of_grace_periods:
                interest = self.get_interest(balance, contract.rate)
                balance += interest

                row = Row(
                    n=period,
                    balance=round(balance, 2),
                    amortization=0,
                    interest=round(interest, 2),
                    payment=0,
                )
            elif period > contract.number_of_grace_periods:
                interest = self.get_interest(balance, contract.rate)
                amortization = payment - interest
                balance -= amortization
                row = Row(
                    n=period,
                    balance=round(balance, 2),
                    amortization=round(amortization, 2),
                    interest=round(interest, 2),
                    payment=round(payment, 2),
                )

            amortization_system.table.append(row)

        return amortization_system

    async def get_constant_amortization_system(
        self: object, contract: AmortizationSystemContract
    ) -> AmortizationSystemContractResponse:
        amortization_system = AmortizationSystemContractResponse(
            system_type=contract.system_type,
            present_value=contract.present_value,
            number_of_periods=contract.number_of_periods,
            rate=contract.rate,
            number_of_grace_periods=contract.number_of_grace_periods,
        )

        if contract.number_of_grace_periods > 0:

            present_value = self.get_future_value(
                present_value=contract.present_value,
                rate=contract.rate,
                number_of_periods=contract.number_of_grace_periods,
            )
            amortization = present_value / contract.number_of_periods
        else:

            amortization = contract.present_value / contract.number_of_periods

        balance = contract.present_value
        total_periods = contract.number_of_periods + contract.number_of_grace_periods
        for period in range(1, total_periods + 1):

            if period <= contract.number_of_grace_periods:
                interest = self.get_interest(balance, contract.rate)

                balance += interest
                row = Row(
                    n=period,
                    balance=round(balance, 2),
                    amortization=0,
                    interest=round(interest, 2),
                    payment=0,
                )

            elif period > contract.number_of_grace_periods:
                interest = self.get_interest(balance, contract.rate)
                payment = amortization + interest
                balance -= amortization

                row = Row(
                    n=period,
                    balance=round(balance, 2),
                    amortization=round(amortization, 2),
                    interest=round(interest, 2),
                    payment=round(payment, 2),
                )
            amortization_system.table.append(row)

        return amortization_system

    async def get_mixed_system(
        self: object, contract: AmortizationSystemContract
    ) -> AmortizationSystemContractResponse:

        amortization_system = AmortizationSystemContractResponse(
            system_type=contract.system_type,
            present_value=contract.present_value,
            number_of_periods=contract.number_of_periods,
            rate=contract.rate,
            number_of_grace_periods=contract.number_of_grace_periods,
        )

        if contract.number_of_grace_periods > 0:
            present_value = self.get_future_value(
                present_value=contract.present_value,
                rate=contract.rate,
                number_of_periods=contract.number_of_grace_periods,
            )
            payment_price = self.get_payment(
                present_value, contract.number_of_periods, contract.rate
            )
            amortization_constant = present_value / contract.number_of_periods
        else:

            payment_price = self.get_payment(
                contract.present_value, contract.number_of_periods, contract.rate
            )
            amortization_constant = contract.present_value / contract.number_of_periods

        balance_price = contract.present_value
        balance_constant = contract.present_value
        balance = contract.present_value
        total_periods = contract.number_of_periods + contract.number_of_grace_periods

        for period in range(1, total_periods + 1):

            if period <= contract.number_of_grace_periods:
                interest = self.get_interest(balance, contract.rate)
                balance += interest
                balance_price += interest
                balance_constant += interest

                row = Row(
                    n=period,
                    balance=round(balance, 2),
                    amortization=0,
                    interest=round(interest, 2),
                    payment=0,
                )

            elif period > contract.number_of_grace_periods:

                interest_price = self.get_interest(balance_price, contract.rate)
                amortization_price = payment_price - interest_price
                balance_price -= amortization_price

                interest_constant = self.get_interest(balance_constant, contract.rate)
                payment_constant = amortization_constant + interest_constant
                balance_constant -= amortization_constant

                payment = (payment_price + payment_constant) / 2
                interest = self.get_interest(balance, contract.rate)
                amortization = payment - interest
                balance -= amortization

                row = Row(
                    n=period,
                    balance=round(balance, 2),
                    amortization=round(amortization, 2),
                    interest=round(interest, 2),
                    payment=round(payment, 2),
                )

            amortization_system.table.append(row)

        return amortization_system
