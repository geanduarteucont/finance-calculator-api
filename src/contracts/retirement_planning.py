from pydantic import BaseModel, Field


class RetirementPlanningContract(BaseModel):
    current_age: int = Field(default=22, gt=0, le=100)
    retiring_age: int = Field(default=65, gt=0, le=100)
    life_expectancy_age: int = Field(default=85, gt=0, le=100)
    expenses_standard_living: float = Field(default=6000, gt=0, le=100000)
    retirement_rate: float = Field(default=0.013)
    investment_rate: float = Field(default=0.005)


class RetirementPlanningContractResponse(RetirementPlanningContract):
    monthly_deposit: float
