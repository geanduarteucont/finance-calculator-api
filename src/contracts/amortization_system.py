from pydantic import BaseModel, Field
from typing import Optional, List
from enum import Enum


class SystemAmortizationType(str, Enum):
    price = "price"
    sac = "constant"
    mixed = "mixed"


class AmortizationSystemContract(BaseModel):
    system_type: Optional[SystemAmortizationType] = Field(default="price")
    present_value: float = Field(default=1000, gt=0, le=1000000)
    number_of_periods: int = Field(default=10, gt=0, le=360)
    rate: float = Field(default=0.10, gt=0, le=1)
    number_of_grace_periods: int = Field(default=0, le=36)


class Row(BaseModel):
    n: int
    balance: float
    amortization: float
    interest: float
    payment: float


class AmortizationSystemContractResponse(AmortizationSystemContract):
    table: List[Row] = []
