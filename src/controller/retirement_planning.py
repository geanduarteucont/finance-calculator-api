from modules import RetirementPlanning
from contracts import RetirementPlanningContract, RetirementPlanningContractResponse


class RetirementPlanningController:
    async def simulate_by_contract(
        self, contract: RetirementPlanningContract
    ) -> RetirementPlanningContractResponse:
        retirement_planning = RetirementPlanning()

        return await retirement_planning.get_monthly_deposits(contract)
