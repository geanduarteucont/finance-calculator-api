from modules import AmortizationSystem
from contracts import AmortizationSystemContract, AmortizationSystemContractResponse


class AmortizationSystemController:
    async def simulate_by_contract(
        self, contract: AmortizationSystemContract
    ) -> AmortizationSystemContractResponse:
        amortization_system = AmortizationSystem()

        if contract.system_type == "price":
            return await amortization_system.get_price_system(contract)
        elif contract.system_type == "constant":
            return await amortization_system.get_constant_amortization_system(contract)
        elif contract.system_type == "mixed":
            return await amortization_system.get_mixed_system(contract)
