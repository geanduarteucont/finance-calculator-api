from tests.utils.setup_tools import SetupTools
import json


class TestAmortizationSystem:
    def setup(self):
        self.endpoint = "/simulate/amortization_system"

    def test_success_price(self, client):
        payload = SetupTools.create_payload_amortization_system()
        payload["system_type"] = "price"
        response = client.post(self.endpoint, data=json.dumps(payload))

        data = json.loads(response.content)
        assert data["table"][-1]["balance"] == 0
        assert response.status_code == 201

    def test_success_constant(self, client):
        payload = SetupTools.create_payload_amortization_system()
        payload["system_type"] = "constant"
        response = client.post(self.endpoint, data=json.dumps(payload))

        data = json.loads(response.content)
        assert data["table"][-1]["balance"] == 0
        assert response.status_code == 201

    def test_success_mixed(self, client):
        payload = SetupTools.create_payload_amortization_system()
        payload["system_type"] = "mixed"
        response = client.post(self.endpoint, data=json.dumps(payload))

        data = json.loads(response.content)
        assert data["table"][-1]["balance"] == 0
        assert response.status_code == 201
