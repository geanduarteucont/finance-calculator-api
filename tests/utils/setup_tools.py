from contracts import AmortizationSystemContract
from contracts.amortization_system import SystemAmortizationType


class SetupTools:
    @staticmethod
    def create_payload_amortization_system():
        return AmortizationSystemContract(
            system_type=SystemAmortizationType("price"),
            present_value=1000,
            number_of_periods=10,
            rate=0.10,
            number_of_grace_periods=0,
        ).dict()
