# FINANCES CALCULATOR API

[Finances Calculator Api](https://finance-calculator-api.herokuapp.com/docs) é uma API Pública para realizar simulações de cálculos financeiros.

Para saber a respeito dos conceitos dessa aplicação, ou também o tutorial de como debugar e testar localmente na IDE [VSCODE](https://code.visualstudio.com/), visite a [WIKI](https://gitlab.com/geanduarteucont/finance-calculator-api/-/wikis) desse repositório.

## ⚙️ Requisitos

Recomendado (principalmente para desenvolvimento local):
- [Python 3.9](https://www.python.org/)
- [Docker\*](https://www.docker.com/)

## 🏃🏽‍♂️ Rodando local

Para rodar o projeto localmente basta executar o script shell (`app.sh`):

sh app.sh start 


Após a execução, a API ficará disponível localmente no endereço http://localhost:8008/docs
## 🚀 Deploy

Hospedado no [Heroku](https://www.heroku.com/). E pode ser acessado no endereço [https://finance-calculator-api.herokuapp.com/](https://finance-calculator-api.herokuapp.com/docs)
Processo de deploy e build são feitos automaticamente após sucesso dos checks do CI/CD do GITLAB na branch `main`.
