#!/bin/sh

start_api()
{
    docker-compose up --build
}

stop_api()
{
    docker-compose -f docker-compose.yml down

}


if [ $1 = "start" ]
then
  start_api

elif [ $1 = "stop" ]
then
  stop_api
  
else
  clear
  echo "Please try again from given options only"
  echo "sh app.sh start"
  echo "sh app.sh stop"
fi